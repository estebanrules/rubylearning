# 3e7.rb
# Your text analyzer will provide the following basic statistics:
# Character count
# Character count (excluding spaces)
# Line count
# Word count
# Sentence count
# Paragraph count
# Average number of words per sentence
# Average number of sentences per paragraph

puts "Which text file would you like to analyze?  Please make sure the file is in the current directory."
file_input = gets.chomp
puts "Analyzing #{file_input}"
lines_of_text = File.readlines(file_input)
count_lines_of_text = lines_of_text.size
text = lines_of_text.join

puts "There are #{count_lines_of_text} lines of text in your file."

count_characters = text.length
puts "There are #{count_characters} characters in your text file."

count_characters_no_spaces = text.gsub(/\s+/, '').length
puts "There #{count_characters_no_spaces} characters (excluding spaces) in your text file."

count_words = text.split.length
puts "There are #{count_words} words in your text file."

count_sentences = text.split(/\.|\?|!/).length
puts "There are #{count_sentences} sentences in your text file."

count_paragraphs = text.split(/\n\n/).length
puts "There are #{count_paragraphs} paragraphs in your text file."

puts "There are an average of #{count_sentences / count_paragraphs } sentences per paragraph in your text file."
puts "There are an average of #{count_words / count_sentences } words per sentence in your text file."
