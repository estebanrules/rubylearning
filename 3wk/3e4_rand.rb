# Week Three Exercise 4

a = (1930...1951).to_a
puts a[rand(a.size)]

# When you run this program, which of the following values will not be displayed?
# 1. 1929
# 2. 1930
# 3. 1945
# 4. 1950
# 5. 1951
# 6. 1952
# Explain why that value will not be displayed.
