# Week Three Exercise Three
# Deaf Grandma
# Whatever you say to grandma (whatever you type in),
# she should respond with HUH?! SPEAK UP, SONNY!, unless you shout it (type in all capitals).
# If you shout, she can hear you (or at least she thinks so) and yells back, NO, NOT SINCE 1938!
# To make your program really believable, have grandma shout a different year each time; maybe any year at
# random between 1930 and 1950. You can't stop talking to grandma until you shout BYE.

bye_count = 0
input = nil
random_year = rand(1965..1972)
puts "What would you like to say to your dear old granny?"

until bye_count == 3
  input = gets.chomp
  if input == 'BYE'
    bye_count += 1
      if input == input.downcase
        puts 'HUH?!?!? SPEAK UP SONNY BOY!!  DID WE LAND ON THE MOON YET??!?!?! THAT STEVE MCQUEEN IS A HANDSOME FELLA, ESPECIALLY WHEN HE\'S SMOKING A VICEROY!!!'
      elsif input == input.upcase
        puts "NO, I HAVEN\'T SEEN RONNY SINCE #{random_year}.  WHO KNOWS, MAYBE ONE DAY HE COULD BE PRESIDENT, AFTER ALL, HE IS ALREADY CORPORATE AMERICA\'S POSTERBOY!!!"
      end
  end
end

∏












