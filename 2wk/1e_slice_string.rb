# Exercise 1
# Write a program that processes the string s = "Welcome to the forum.\nHere you can learn Ruby.\nAlong with other members.\n"
# a line at a time, using all that we have learned so far. The expected output is:
# >ruby tmp.rb
# Line 1: Welcome to the forum.
# Line 2: Here you can learn Ruby.
# Line 3: Along with other members.
# >Exit code: 0

string_to_slice = "Welcome to the forum.\nHere you can learn Ruby.\nAlong with other members.\n"

line_one = string_to_slice.slice(0, 22)
line_two = string_to_slice.slice(22, 25)
line_three = string_to_slice.slice(-26, 26)

print 'Line 1: ' + line_one
print 'Line 2: ' + line_two
print 'Line 3: ' + line_three
