# Exercise3. Write a method leap_year?. It should accept a 
# year value from the user, check whether it's a leap year, and then 
# return true or false. With the help of this leap_year?() method 
# calculate and display the number of minutes in a leap year 
# (2000 and 2004) and the number of minutes in a non-leap year 
# (1900 and 2005). Note: Every year whose number is divisible by four 
# without a remainder is a leap year, excepting the full centuries, which,
# to be leap years, must be divisible by 400 without a remainder. If not 
# so divisible they are common years. 1900, therefore, is not a leap year.


def leap_year?(leap_test)
  leap_test = case 
    when leap_test % 400 == 0 then true
    when leap_test % 100 == 0 then false
    else leap_test % 4 == 0
    end
  return leap_test
end

def calc_minutes(year_type, year)
  minutes_in_year = 365 * 24 * 60
  minutes_in_leap_year = 366 * 24 * 60
  if year_type == true
    puts "In the year #{year}, there were #{minutes_in_leap_year} minutes."
  elsif year_type == false
    puts "In the year #{year}, there were #{minutes_in_year} minutes."
  end
end

puts "Please input a year"
check_year = gets.chomp
leap_result = leap_year?(check_year.to_i)

if leap_result == true
  puts "Congratulations!  That's a leap year!"
else
  puts "No dice.  That is not a leap year."
end

calc_minutes(leap_result, check_year)