# This program greets Life, the Universe and Everything
#     doctest: I can greet the world
#     >> hello
#     => "Hello, World!"
#     doctest: I can greet someone personally
#     >> hello "Billy"
#     => "Hello, Billy!"
#     doctest: I can greet someone else personally
#     >> hello "Someone else"
#     => "Hello, Someone else!"
#     doctest: I can find out if someone is there
#      This one is actually less straight forward
#     >> hello("are you there", "?")
#     => "Hello, are you there?"
#     doctest: I can find out if Joe is there
#     >> hello("Joe", "?")
#     => "Hello, Joe?"
#     doctest: I can find out if Joe is there
#     >> hello("Joe", "?")
#     => "Hello, Joe?"

def hello(name = 'World', punctuation = '!')
  "Hello, #{name}#{punctuation}"
end

