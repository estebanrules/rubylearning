# 6e1.rb
# Write a class called Person that has balance
# as an instance variable and make it readable via an accessor.

class Person
  def initialize(name, balance)
    @name = name
    @balance = balance
  end
  attr_reader :balance
end

person1 = Person.new("Nero", "unstable")

puts person1.balance


