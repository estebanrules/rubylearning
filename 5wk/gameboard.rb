class GameBoard

  def set_locations_cells(locations)
    @locations = locations
  end

  def check_yourself(user_guess)
    @user_guess = user_guess
    if @locations.include?(user_guess) == true
      puts "You guessed correctly!"
    else
      puts "Incorrect."
    end
  end
end
