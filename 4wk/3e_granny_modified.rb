# Modified Deaf Grandma
# This time as a class

class Granny
  attr_reader :deafness_level, :name
  def initialize(name = 'Juliana', hearing = 3, years_remembered = (1930..1950))
    @name = name
    @deafness_level = hearing
    @bye_count = 0
    @years = years_remembered
  end

  def year_remembered
    @years.to_a.sample
  end

  def greeting(message = "Hello Sonny.  My durablecells have run out for my ear assistants!")
    message
  end

  def bye?
    @bye_count >= deafness_level
  end

  def responds(message)
    manage_bye_count(message)
    granny_says(message)
  end

  def shouted?(message)
    message == message.upcase
  end

  def granny_says(message)
    shouted?(message) ? "NO, NOT SINCE #{year_remembered}!" : 'HUH?! SPEAK UP SONNY!'
  end

  def manage_bye_count(message)
    if message == 'BYE'
      @bye_count = @bye_count + 1
    else
      @bye_count = 0
    end
  end
end

def sonny_says
   gets.chomp
end

if __FILE__ == $PROGRAM_NAME
  granny = Granny.new
  puts "You have arrived at #{granny.name}'s casa!'"
  puts granny.greeting
  until granny.bye?
    print granny.greeting("What do you want? -> ")
    puts granny.responds(sonny_says)
  end
end
