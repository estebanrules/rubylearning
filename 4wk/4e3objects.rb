# Modified Deaf Grandma, this time she is a class!
class Granny
  def initialize(name)
    @name = name
    @bye_count = 0
    @input = nil
    puts "You dear old Grandmother #{@name} is here."
  end

  def year
    (1930..1950).to_a.sample
  end

  def get_input
    puts "What would you like to say to Granny?"
    @input = gets.chomp
  end

  def check_case
    while @bye_count < 3
      get_input
      if @input == @input.upcase
          puts "NO I HAVEN'T SEEN BILLY SINCE #{year}  I THINK HE WENT TO MARS."
      elsif @input == @input.downcase
          puts "WHAT?!?!?!?  SPEAK UP!  I CANT BELIEVE IT'S ALREADY 1922!"
      end
    check_count
    end
  end

  def check_count
    if @input == "BYE"
      @bye_count = @bye_count + 1
    else
      @bye_count = 0
    end
  end
end

granny = Granny.new('Matilda')

granny.check_case






