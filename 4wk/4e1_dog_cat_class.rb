# Write a class called Dog, that has name as an instance variable and the following methods:
# bark(), eat(), chase_cat()

class Dog
  def initialize(dog_name)
    @name = dog_name
  end

  def get_name()
    return @name
  end

  def bark(barking)
    return barking
  end

  def eat(eating)
    return eating
  end

  def chase_cat(chasing)
    return chasing
  end
end

dog_one = Dog.new('Buster')
puts dog_one.bark('Bark, bark! bark!')
puts dog_one.eat('Me see food.  Me eat food.  Me sleep now.')
puts dog_one.chase_cat('I hate cats!!! I have no idea why!!! I\'m going to chase them!!!')

puts 'What\'s your name, boy?'

puts "My name is #{dog_one.get_name}."
