# YOU CANNOT PASS METHODS INTO OTHER METHODS (BUT YOU CAN PASS PROCS INTO METHODS)
# METHODS CANNOT RETURN OTHER METHODS (BUT THEY CAN RETURN PROCS)

# This example shows HOW METHODS CAN TAKE PROCS:
def some_method some_proc
  puts 'Start of method'
  some_proc.call
  puts 'End of method'
end

say = lambda do
  puts 'Hello'
end

some_method say
# output is:
# Start of mtd
# Hello
# End of mtd

#-----------------------------------------------------------------------------------------------


# Another example
a_Block = lambda { |x| "Hello #{x}!"}
puts a_Block.call 'World!'

# output is:
# Hello World!
