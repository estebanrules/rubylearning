# Regular Expressions, through cryptic, is a powerfuk tool for working with text.
# Many people find them difficult to use, difficult to read, un-maintainable.
# See tutorial for further comments.

# A regular expression is simply a way of specifying a pattern of characters to be matched
# in a string.

# In Ruby you typically create a reg exp by writing a pattern between slash characters.
# i.e., /pattern/
# In Ruby, regular expressions are objects of type of Regexp (duh) and can be manipulated as such.
# // is a regexp and an instance of the Regexp class. as show below:

//.class # Regexp

#You could write a pattern that matches a string containing the text Pune or the text Ruby using the following
# regular expression:
/Pune|Ruby/
# The pipe character (|) means "either the thing on the right or the left"
# The simplest way to find out whether there's a match between a string and a pattern
# is with the match method.

# If there's no match, you get back nil
# If there's a match, it returns an instance of the class MatchData
# We can also use the match operator =~ to match a string against
#  a regular expression. If the pattern is found in the string, =~ returns its starting position, otherwise it returns nil.
m1 = /Ruby/.match("The future is Ruby")
puts m1.class # it returns MatchData
m2 = "The future of Ruby" =~ /Ruby/
puts m2       # it returns 14

# Some characters have special meanings to the regexp parser.  When you want to match one of these special characters as itself,
# you have to escape it with a backslash.

# For example:  to match the character '?' :
/\?/
# The backslash is an escape sequence
# The special characters you have to do this for are: ^, $, ? , ., /, \, [, ], {, }, (, ), +, and *

# Wildcard
# Sometimes you'll want to match any character at some point in your pattern.  You do this with the special wildcard
# character '.' (dot).
# This would match both:
/.ejected/
# "dejected" and "rejected"

# Character Classes
# a character class is an explicit list of characters, placed inside the regular expression in square brackets.
/[dr]ejected/
# this means 'either d or r, followed by ejected.'  This new pattern matches either "dejected" or "rejected" but not "&ejected"

# You can insert a range of characters:
/[a-z]/

# To match a hexidec digit you might use several ranges inside a character class:
/[A-Fa-f0-9]/

# To match any digit:
/[0-9]/

# But you can also accomplish this more concisely with the special escape sequence \d:
/\d/ # THINK:  decimal.

# Also:
/\w/ # matches any digit, alphabetical character or underscore (_)
/\s/ # matches any whitespace character (space, tab, newline)

# NEGATED FORM:
# Each of these character classes also has a negated form:
/\D/
/\W/
/\S/
# THESE MATCH ANY CHARACTER OTHER THAN WHAT THE LOWERCASE FORM MATCH.

#---------------------------------------------------------------------------------------------------------------
# Every match operation either succeeds or fails.  Let's start with the simpler case:  failure!
# When you try to match a string to a pattern, and the string doesn't match, the result is always nil:
/a/.match("b") # nil, the 'nil' stands in for the false.

/a/.match("a") # true

# Unlike nil, the MatchData object returned by a successful match has a Boolean value of true, which makes it usrul for simple
# match/no-match tests.

# o use the MatchData object, you must first save it.
# Consider an example where we want to pluck a phone number
# from a string and save the various parts of it
# (area code, exchange, number) in groupings.
# Example:



















































