# "An object is an entity that serves as a container for data and also
# controls access to the data. Associated with an object is a set of
# attributes, which are essentially no more than variables belonging to that object.
#  Also associated with an object is a set of functions that provide an interface to the
# functionality of the object, called methods."
#                                              - Hal Fulton


class Dog
  def initialize(breed, name)
    # Instance variables
    @breed = breed
    @name = name
  end

  def bark
    puts 'Ruff! Ruff!'
  end

  def display
    puts "I am of the Royal House of #{@breed} and my fucking name is #{@name} so don't fuck with me"
  end
end

# make an object
# Objects are created on the heap
d = Dog.new('Labrador', 'Benzy')

=begin
  Every object is "born" with certain innate abilities.
  To see a list of innate methods, you can call the methods
  method (and throw in a sort operation, to make it
  easier to browse visually). Remove the comment and execute.
=end

puts d.methods.sort

# To know whether the object knows how to handle the message you want
# to send it, by using the respond_to? method.
if d.respond_to?("talk")
  d.talk
else
  puts "Sorry, d doesn't understand the 'talk' message"
end

d.bark
d.display

# making d and d1 point to the same object
d1 = d
d1.display

# making d a nil reference, meaning it does not refer to anything
d = nil
d.display

# If I now say
d1 = nil
# then the Dog object is abandoned and eligible for Garbage Collection (GC)

d2 = Dog.new('Alsatian', 'Lassie')
puts d.class.to_s






















































