# Every match operation either succeeds or fails.  Let's start with the simpler case:  failure!
# When you try to match a string to a pattern, and the string doesn't match, the result is always nil:
/a/.match("b") # nil, the 'nil' stands in for the false.

/a/.match("a") # true

# Unlike nil, the MatchData object returned by a successful match has a Boolean value of true, which makes it usrul for simple
# match/no-match tests.

# To use the MatchData object, you must first save it.
# Consider an example where we want to pluck a phone number
# from a string and save the various parts of it
# (area code, exchange, number) in groupings.
# Example:

# Here we use the string method
string = "My phone number is (123) 555-1234."
phone_re = /\((\d{3})\)\s+(\d{3})-(\d{4})/
# Here we use the MatchData object.
m = phone_re.match(string)
unless m
  puts "There was no match..."
  exit
end
print "The whole string we started with: "
puts m.string
print "The entire part of the string that matched: "
puts m[0]
puts "The three captures: "
# Here we use the 'times' method
3.times do |index|
  puts "Capture ##{index + 1}: #{m.captures[index]}"
end
puts "Here's another way to get at the first capture:"
print "Capture #1: "
puts m[1]
