# BLOCKS ARE NOT OBJECTS, but they can be converted into objects of class Proc.
# This can be done by calling the lambda method of the class Object.
# A block created with lambda acts like a Ruby method.
# If you don't specify the right number of arguments, you can't call the block.

# prc = lambda {"hello"}

# Proc objects are blocks of code that have been bound to a set of variales.  The class Proc
# has a method call that invokes the block.
# Example:

# Blocks are not objects
# they can be converted into objects of class Proc by calling lambda method
prc = lambda {puts 'Hello'}
# method call invokes the block
prc.call

# another example
toast = lambda do
  'Cheers'
end
puts toast.call


