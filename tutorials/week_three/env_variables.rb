# Environment Variables
# an environment variable is a link between our program and the outside world.
# It is a reference to a piece of text, and can be used to store configuration
# information such as paths, usernames, etc.  You can access operating system
# variables using the predefined variable ENV.  ENV is simply a hash.

ENV.each {|k,v| puts "#{k}: #{v}"}
