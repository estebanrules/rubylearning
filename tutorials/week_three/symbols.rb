# Symbol
# A symbol looks like a variable name bit it's prefixed with a colon.


# A symbol is the most basic Ruby object you can create.  It's just a name and 
# an internal ID.  Symbols are useful because a given symbol name refers to the
# same object throughout a Ruby program.

# use the object_id method of class object
# it returns an integer identifier for an object
puts "string".object_id
puts "string".object_id
puts :symbol.object_id
puts :symbol.object_id

# output:
# 70352915172700
# 70352915172600
# 385448
# 385448


