# Here is an interesting example of a method that returns an array:
# if you give return multiple parameters, the method returns them in an array
# The times method of the Integer class iterates block num times,
# passing in values from zero to num -1

def mtdarry
  10.times do |num|
    square = num * num
    return num, square if num > 5
  end
end

num, square = mtdarry
puts num
puts square

# Parallel Assignment
# lvalue - "something" that can appear on its own on the lefthand side of an assignment,
# i.e. a variable, a constant, or attr setter method.
#
# rvalue - "something" that can appear on its own on the right hand side
# Ruby lets you have a comma-separated list of values.  Once Ruby sees more than
# one value in an assignment, the rules of parallel assignment come in to play.

# First, all of the rvalues are evaluated and collected in to an array (left to right)
# Next, the lvalue (left hand side) is inspected.  If it contains a single element, 
# the array is assigned to that element.

# I GET IT.  LOOK BELOW, BITCHES!

# So you have this ('non-parallel' assignment):
a = 1, 2, 3, 4 # => a == [1, 2, 3, 4]
b = [1, 2, 3, 4] # => b == [1, 2, 3, 4] 

# And this, parallel assignment:
a, b = 1, 2, 3, 4 # => a == 1, b == 2  
c, = 1, 2, 3, 4 # => c == 1
# **IF THE LEFT HAND SIDE HAS A COMMA, RUBY MATCHES VALUES ON THE RIGHT HAND SIDE
# AGAINST SUCCESSIVE ELEMENTS ON THE LEFT HAND SIDE.
# IT'S ALL ABOUT THE COMMA, BABY!