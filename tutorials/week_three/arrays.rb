# Arrays.  Yay!

# Empty Array
var1 = []
# Array index starts from 0
puts var1[0]

# An array holding a single number
var2 = [5]
puts var2[0]

# an array holding two strings
var3 = ['Hello', 'Goodbye']
puts var3[0]
puts var3[1]

# an array whose objects are pointing to three objects - 
# a float, a string and an 
flavour = 'mango'
var4 = [80.5, flavour, [true, false]]
puts var4[2]

# a trailing comma is ignored
name = ['Satish', 'Talim', 'Ruby', 'Java',]
puts name[0]
puts name[1]
puts name[2]
puts name[3]
# the next output is nil, which is Ruby's way of saying "nothing"
puts name[4]

# we can add more elements too
name[4] = 'Pune'
puts name[4]
# we can add anything!
name[5] = 4.33
puts name[5]
# we can addan array to an array, mofos!
name[6] = [1, 2, 3]
puts name[6]

# some methods on arrays
newarr = [45, 23, 1, 90]
puts newarr.sort
puts newarr.length
puts newarr.first
puts newarr.last

# method each (iterator) - extracts each element into loc
# 'do - end' is a block of code
# variable loc refers to each item in the array as it ges through the loop.
locations = ['Krypton', 'Oa', 'Mars']

locations.each do |loc|
  puts 'I love ' + loc + ' this time of year!'
  puts 'Don\'t you?'
end

# *Above: The variable 'loc' inside the goalposts refers to each item in the array
# as it goes through the loop.
# *Above: The 'do' and 'end' identify a block of code that will be executed
# for each item.  Blocks are used extensively in Ruby.


































