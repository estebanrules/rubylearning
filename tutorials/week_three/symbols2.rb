# When do we use a string vs. a symbol?

# if the contents of the object are important, use a string
# if the identity of the object is important, use a symbol

# See attachments about Symbols
class Test
  puts :Test.object_id.to_s
  def test
    puts :test.object_id.to_s
    @test = 10
    puts :test.object_id.to_s
  end
end

t = Test.new
t.test

# Another example: 
# (in this case it seems like the symbol :yes is used as a conditional)
# In this example, :yes is a symbol.  Symbols don't contain values or objects, like 
# variables do.  They are used as a consistent name within code.  
know_ruby = :yes
if know_ruby == :yes
  puts 'You are a Rubyist, mofo!'
else
  puts 'Best start learning Ruby, dumbass.'
end

# For example, you could have easily replaced the symbol with a string, as in
# this example:
know_ruby1 = 'yes'
if know_ruby1 == 'yes'
  puts 'You are a Rubyist, mofo!'
else
  puts 'Start learning Ruby'
end
# The above creates the same result but is not as efficient.  In this example,
# every mention of 'yes' creates a new object stored separately in memory, whereas
# symbols are single references values that are only initialized once.  In the first 
# example, only :yes exists, whereas in the second example you end up with the 
# full strings of 'yes' and 'yes' taking up memory.

# We can also tranform s String into a Symbol and vice-versa:
puts "string".to_sym.class # Symbol
puts :symbol.to_s.class # String

































