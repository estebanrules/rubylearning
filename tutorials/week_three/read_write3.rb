# The File.open method can open the file in different modes
# like 'r' Read-only, starts at the beginning of file (default)
# 'r+', Read/Write, starts at beginning of the file, 
# 'w'. Write-only, truncates existing file to zero length or creates a new
# file for writing.

# File modes work like so:
# <file mode><:external encoding><:internal>

# Example:
# File.open('p014constructs.rb', 'r:UTF-16LE:UTF-8') do |f1|
# See attached for more information.

# Traversing Directory Trees
# The Find module supports top-down traversal of a set of file paths, 
# given as arguments to the find method.
# If an argument is a directory, then its name and name of all its 
# files and sub-directories will be passed in (in the example below, this would
# be from where you run this program).
require 'find'  
Find.find('./') do |f|  
  type = case  
         when File.file?(f) then "F"  
         when File.directory?(f) then "D"  
         else "?"  
         end  
  puts "#{type}: #{f}"  
end

# Random Access
# lets say we have a text file (named hellousa.rb) and the content is:
#     puts 'Hello USA'
# But now, we want to display the contens of the file from the word USA (onward):
f = File.new("hellousa.rb")

# SEEK_CUR - Seeks to first integer number parameter plus current position  
# SEEK_END - Seeks to first integer number parameter plus end of stream  
#   (you probably want a negative value for first integer number parameter)  
# SEEK_SET - Seeks to the absolute location given by first integer number parameter  
# :: is the scope operator - more on this later
f.seek(12, IO::SEEK_SET)
print f.readline
f.close

__END__

Ruby supports the notion of a file pointer. The file pointer indicates the current location in the file.
The File.new method opens the file 'hellousa.rb' in read-only mode (default mode), returns a new File object and the 
file pointer is positioned at the beginning of the file. In the above program, the next statement is f.seek(12, IO::SEEK_SET). 
The seek method of class IO, moves the file pointer to a given integer distance (first parameter of seek method) in the stream 
according to the value of the second parameter in the seek method.

IO::SEEK_CUR - Seeks to first integer number parameter plus current position
IO::SEEK_END - Seeks to first integer number parameter plus end of stream (you probably want a negative value for first integer number parameter)
IO::SEEK_SET - Seeks to the absolute location given by first integer number parameter
More on the scope operator :: to come later.

Does Ruby allow Object Serialization?

Java features the ability to serialize objects, letting you store them somewhere and 
reconstitute them when needed. Ruby calls this kind of serialization marshaling. Saving an object and 
some or all of its components is done using the method Marshal.dump. Later on you can reconstitute the 
object using Marshal.load. Ruby uses marshaling to store session data.
