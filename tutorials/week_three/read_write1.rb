# Let's see how we can read / write to a text file 
# with the help of a simple program

# Open and read from a text file
# Note that since a block is given, file will
# automatically be closed when the block terminates

# Here we instantiate a new file.
File.open('randomnum.rb', 'r') do |f1|
  while line = f1.gets
    puts line
  end
end

