# Ruby Hashes
# Hashes are similar to arrays in that they are INDEXED collection of
# object references.

# This example uses 'hash literals': a list of key => pairs between braces.
h = {'dog' => 'canine', 'cat' => 'feline', 'donkey' => 'asinine', 12 => 'dodecine'}
puts h.length   # 4
puts h['dog']   # canine
puts h          # {"dog"=>"canine", "cat"=>"feline", "donkey"=>"asinine", 12=>"dodecine"}
puts h[12]      # dodecine

# Output:
 
# canine  
# {"dog"=>"canine", "cat"=>"feline", "donkey"=>"asinine", 12=>"dodecine"}  
# dodecine

# Using symbols as keys:
# This is one of many ways to instantiate a Hash.
people = Hash.new
people[:nickname] = 'IndianGuru'
people[:language] = 'Marathi'
people[:lastname] = 'Talim'

puts people[:lastname] # Talim

# Another example: 
h = {:nickname => 'IndianGuru', :language => 'Marathi', :lastname => 'Talim'}  
puts h 

# The output is:
# {:nickname=>"IndianGuru", :language=>"Marathi", :lastname=>"Talim"} 

# Another example (if using symbols):
h = {nickname: 'IndianGuru', language: 'Marathi', lastname: 'Talim'}  
puts h

# output:
# {:nickname=>"IndianGuru", :language=>"Marathi", :lastname=>"Talim"}

# An exception to the shorter (symbol: value) syntax is when you would like
# to use a numeric key:
hash = {1: 'one'} # Will not work
hash = {1 => 'one'} # will work









































