# How do I convert objects in to an Array?
# If you want to wrap objects in to an Array, you can use a special Kernel
# module Array method.
# Example:

str = 'hello'
print Array(str).class # Array

# Another example:
str1 = 'hello\nworld'  
print Array(str1) # ["hello\\nworld"] 

# What are the ancestors of Array? Run the following program, to find that out:

a = [1,2,3,4]  
print a.class.ancestors  
# You should see:

# [Array, Enumerable, Object, Kernel, BasicObject]