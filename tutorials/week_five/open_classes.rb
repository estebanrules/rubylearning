# In Ruby, classes are never really 'closed'.  You can always add methods to an
# existing class.  This applies to both YOUR classes and the built-in, standard
# classes of ruby.  You only have to open up a class definition  for an
# existing class, and the new contents you specify will be added to whatever
# is there
require_relative 'incl_files'
m = MotorCycle.new('Yamaha', 'red')
m.start_engine

class MotorCycle
  def disp_attr
    puts 'Color of MotorCycle is ' + @color
    puts 'Make of MotorCycle is ' + @make
  end
end

m.disp_attr
m.start_engine
puts self.class
puts self
# Note that self.class refers to Object
# and self refers to an object called main of class Object
