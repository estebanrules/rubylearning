# Here, we would like to add a 'write_size' method to the String class
# bit first, we will get a list of all the methods in Ruby's standard
# String class that begins with wr:
String.methods.grep /^wr/ # []

# From this we can see that the String class has no method starting with 'wr'

# Here we add out new method:
class String
  def write_size
    self.size
  end
end
size_writer = "Tell me my size!"
puts size_writer.write_size

puts size_writer.size
