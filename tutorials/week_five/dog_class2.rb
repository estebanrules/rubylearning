require_relative 'dog_class1'
# define class Dog (really we are modifying it)

class Dog
  def big_bark
    puts 'Woof, Woof, Bitch-Ass!'
  end
end
# make an object
d1 = Dog.new('Labrador', 'Dickhead')
d1.bark
d1.big_bark
d1.display
# In the above example, because we include the ruby file
# 'dog_class1', we get access to all the methods from the class
# definition in that file, and at the same time we can modify that class,
# as we have done above (we added a new method 'big_bark') but at the same time
# we also used the class methods from the file that we 'required'

