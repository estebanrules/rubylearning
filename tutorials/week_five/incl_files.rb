# At first you put all your code in one file, eventually your programs grow, etc, etc.

# In order to do this, you need to use Ruby's require and load methods.  (both are global functions defined in Onject)
# but are used like language keywords, that help you include other files in your program.

# The load  method includes the named Ruby source file every time the method is executed.

# load 'filename.rb'

# The more commonly used require method loads any given file only once

# require 'filename'

# Some extensions (for example) are written in C
# FYI:  RAILS USES LOAD IN PREFERENCE TO REQUIRE.
# SEE TUTORIAL FOR MORE INFORMATION.

class MotorCycle
  def initialize(make, color)
    # Instance variables
    @make = make
    @color = color
  end

  def start_engine
    if @engine_state
      puts 'Engine is already Running'
    else
      @engine_state = true
      puts 'Engine Idle'
    end
  end
end




