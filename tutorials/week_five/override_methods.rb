# Method overriding, in object oriented programming, is a language feature that
# allows a subclass to provide a specific implementation of a method that is already provided
# by one of its superclasses.
# The implementation in the subclass overrides (replaces) the implementation in the superclass.

class A
  def a
    puts 'In class A'
  end
end

class B < A
  def a
    puts 'In class B'
  end
end

b = B.new
b.a

# Usage of 'super'
# How 'super' handles arguments:
# **READ ATTACHED DOCUMENTATION**

class Bicycle
  attr_reader :gears, :wheels, :seats
  def initialize(gears = 1)
    @wheels = 2
    @seats = 1
    @gears = gears
  end
end

class Tandem < Bicycle
  def initialize(gears)
    super
    @seats = 2
  end
end

# Here we instantiate our Tandem (sub-class of class Bicycle)
t = Tandem.new(2)
puts t.gears
puts t.wheels
puts t.seats
b = Bicycle.new
puts b.gears
puts b.wheels
puts b.seats

# Output:
# >ruby p038bicycle.rb
# 2
# 2
# 2
# 1
# 2
# 1
#----------------------------------------------------------------------------------------------
# Redefining Methods
class OR
  def mtd
    puts "First definition of method mtd"
  end
  def mtd
    puts "Second method of method mtd"
  end
end
OR.new.mtd
OR.new.mtd
# **SEE DOCUMENTATION**
# The printed result is the Second definition of method mtd.
# The second definition has prevailed: We see the output from that definition,
# not from the first. Nothing stops you from defining a method twice,
# however the new version takes precedence.
#----------------------------------------------------------------------------------------------------------

# Abstract Class
# In Ruby, we can define an abstract class that invokes certain undefined
# "abstract" methods, which are left for subclasses to define. For example:

# This class is abstract; it doesn't define hello or name
# No special syntax is required: any class that invokes methods
# that are intended for a subclass to implement is abstract
class AbstractKlass
  def welcome
    puts "#{hello} #{name}"
  end
end

# A concrete class
class ConcreteKlass < AbstractKlass
  def hello; "Hello"; end
  def name; "Ruby students"; end
end

ConcreteKlass.new.welcome # Displays "Hello Ruby students"

# ** SEE ACCOMPAMIED DOCUMENTATION **













































