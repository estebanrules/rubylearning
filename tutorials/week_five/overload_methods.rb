# Overloading methods in Ruby

# Case:  You want to create two different versions of a method with the same name:
# two methods that differ in the arguments they take.
# However, a Ruby class can only have one method name given (see previous code example)
# **WITHIN THAT SINGLE METHOD HOWEVER, YOU CAN PUT LOGIC THAT BRANCHES DEPENDING ON
# HOW MANY AND WHAT KINDS OF OBJECTS WERE PASSED IN AS ARGUMENTS**

# Example:  Here's a Rectangle class that represents a rectangular shape on a grid.
# You can instantiate a Rectangle by one of two ways:
# 1 - by passing in the coordinates of its top-left and bottom-right corners
# 2 - by passing in its top-left corner along with its length and width.

# There's only one initialize method, but you can act as though there were two.

# The Rectangle constructor accepts arguments in either
# of the following forms:
#   Rectangle.new([x_top, y_left], length, width)
#   Rectangle.new([x_top, y_left], [x_bottom, y_right])
class Rectangle
  def initialize(*args)
    if args.size < 2 || args.size > 3
      # modify this to raise exception, later
      puts 'This method takes wither 2 or 3 arguments'
    else
      if args.size == 2
        puts 'Two Arguments'
      else
        puts 'Three Arguments'
      end
    end
  end
end
Rectangle.new([10, 23], 4, 10)
Rectangle.new([10, 23], [14, 13])

# output:
# Three Arguments
# Two Arguments

__END__

GO OVER DOCUMENTATION, WRITE YOUR OWN EXAMPLES.

