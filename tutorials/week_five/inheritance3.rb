# There are many classes and modules (more on this later) built into the standard Ruby language. 
# They are available to every Ruby program automatically; no require is required. Some built-in 
# classes are Array, Bignum, Class, Dir, Exception, File, Fixnum, Float, Integer, IO, Module, Numeric, 
# Object, Range, String, Thread, Time. Some built-in modules are Comparable, Enumerable, GC, Kernel, Math.

# The BasicObject class is the parent class of all classes in Ruby. Its methods are therefore available to all 
# objects unless explicitly overridden. Prior to Ruby 1.9, Object class was the root of the class hierarchy. 
# The new class BasicObject serves that purpose, and Object is a subclass of BasicObject. BasicObject is a very 
# simple class, with almost no methods of its own. When you create a class in Ruby, you extend Object unless you explicitly 
# specify the super-class, and most programmers will never need to use or extend BasicObject.

# In Ruby, initialize is an ordinary method and is inherited like any other.
#                               
# ***IN RAILS: Inheritance is one of the key organizational techniques for Rails program design and the design of the Rails framework.***

# usage of the super method
class Dog
  def initialize(breed)
    @breed = breed
  end

  def to_s
    "(#@breed, #@name)"
  end
end

class Lab < Dog
  def initialize(breed, name)
    super(breed)
    @name = name
  end
end

puts Lab.new("Labrador", "Benzy").to_s # .to_s is called implicitly with any puts or print or p
                                       # method call

# When you invoke super with arguments, Ruby sends a message to the parent of the current object, asking it to invoke a method of 
# the same name as the method invoking super. super sends exactly those arguments.

# The to_s method in class Lab references @breed variable from the super-class Dog. This code works as you probably expect it to:
#               puts Lab.new("Labrador", "Benzy").to_s ==> (Labrador, Benzy)                                        

# BECAUSE THIS CODE BEHAVES AS EXPECTED, YOU MAY BE TEMPTED TO SAY THAT THESE VARIABLES ARE INHERITED.
#               GUESS WHAT?
# THAT IS NOW HOW RUBY WORKS.
# So, how does it work?

# All Ruby objects have a set of instance variables.  There are NOT defined by the object;s class - they are simply created when a value 
# is assigned to them.  Because instance variables are not defined by a class, they are unrelated to subclassing and the inheritance mechanism.

# See attached tutorial. 