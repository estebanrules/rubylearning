# Ruby Inheritance
# Inheritance is a relation between two classes
# We know that all cats are mammals, and all mammals are animals
# The benefit of inheritance is that classes lower down the hierarchy get the
# features of those higher up, but can also add specific features of their own.
# If all mammals breathe, then all cats breathe. In Ruby, a class can only
# inherit from a single other class. Some other languages support multiple
#  inheritance, a feature that allows classes to inherit features from multiple
#  classes, but Ruby doesn't support this.
# Example:
# Inheritance
class Mammal
  def breathe
    puts "inhale and exhale"
  end
end
# In the above example, class Mammal is the 'base class', and has the class
# method 'breathe'.

#  In the below example, class Cat is a 'sub-class' of class Animal (after all,
# cats are animals.)  class Cat 'inherits' the class method 'breathe' from
# class Mammal.
class Cat < Animal
  def speak
    puts "Meow"
  end
end
# Below we are instantiating a new object from class Cat.
# We call the class method breathe, because class Cat has
# inherited the method from class Mammal
rani = Cat.new
rani.breathe
rani.speak

# There are certain situations in which certain properties of the super-class
# should not be inherited by a particular subclass.
# Though birds (generally) know how to fly, penguins are a flightless subclass
# of birds.
# In the example below, we override the method fly in class Penguin.
class Bird
  def preen
    puts "I am cleaning my feathers."
  end
  def fly
    puts "I am flying."
  end
end

class Penguin < Bird
  def fly
    puts "Sorry.  I'd rather swim."
  end
end

p = Penguin.new
p.preen
p.fly
# Rather than exhaustively define every characteristic of every new
# class, we need only to append or to redefine the differences
# between each subclass and its super-class.  The use of inheritance
# is sometimes called differential programming.  It is one of the
# benefits of object-oriented programming.
