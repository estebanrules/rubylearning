# Example for protected access control
class Person
  def initialize(age)
    @age = age
  end
  def age
    @age
  end
  def compare_age(c)
    if c.age > age
      "The other object's age is bigger."
    else
      "The other object's age is the same or smaller."
    end
  end
  protected :age
end

chris = Person.new(25)
marcos = Person.new(34)
puts chris.compare_age(marcos)
# puts chris.age

# if you call the above method, you get the following error:
# access_control3.rb:22:in `<main>': protected method `age'
# called for #<Person:0x0000010180fc58 @age=25> (NoMethodError)

__END__

SEE DOCUMENTATION
