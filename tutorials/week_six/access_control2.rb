# Alternatively, you can set access levels of named methods by listing them as
# arguments to the access control functions.
class ClassAccess
  def m1   # public method
  end
  public :m1
  protected :m2, :m3
  private :m4, :m5
end
