# Access Control
# The only easy way to change an object's state in Ruby is
# by calling one of its methods. Control access to the methods,
# and you have controlled access to the object. A good rule of
# the thumb is never to expose methods that could leave an object
# in an invalid state.

# Ruby gives you three levels of protection:
#
# 1 - Public methods can be called by everyone - no access control
# is enforced. A class's instance methods
# (these do not belong only to one object; instead, every instance
# of the lass can call them) are public by default; anyone can call them.
# The initialize method is always private.
#
# 2 - Protected methods can be invoked only by objects of the defining
# class and its subclasses. Access is kept within the family.
# However, usage of protected is limited.
#
# 3 - Private methods cannot be called with an explicit receiver - the
# receiver is always self. This means that private methods can be called
# only in the context of the current object; you cannot invoke another
# object's private methods.
#---------------------------------------------------------------------
# Example of an access violation (when the attempts to execute
# the restricted method.)
class ClassAccess
  def m1    # public method
  end

  protected
    def m2  # protected method
    end

  private
    def m3  # private method
    end
end

ca = ClassAccess.new
ca.m1
# ca.m2  => if you called this, you would get an access violation runtime
# ca.m3  => if you called this, you would get an access violation runtime
#-----------------------------------------------------------------------































