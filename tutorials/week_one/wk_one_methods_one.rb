# Some useful Ruby methods

# One output method we use a lot is 'printf', which prints
# its arguments under the control of a format string.

printf("Number: %5.2f,\nString: %s\n", 7.18, "FORPC101")

# The output is:

# Number: 7.18,
# String: FORPC101

# We also use step

# class Numeric also provides the more general method 'step',
# which is more like a traditional 'for loop'.

# Example:

8.step(40,5) {|i| print i, " "}

# This produces:
# 8 13 18 23 28 33 38


