# Fun with Strings

# Ruby Strings
=begin
Ruby Strings
In Ruby, string are mutable
=end

puts "Hello Dickhead!"

# Can use '' or " " for Strings
puts 'Hello Dumbass!'

# String Concatenation
puts 'I like ' + 'Ruby'

# Escape Sequences (like python!)
puts "It\'s my Ruby, bitch!"

# New here, displays the string three times
puts "Hello " * 3

# Defining a constant (this is not the preferred way to declare a var in ruby)
PI = 3.1416
puts PI

# Everything in Ruby is an object!!!!
# If puts is passed an object that is NOT a string, puts calls the to_s
# method of that object and prints the string returned by that method.

# Special kind of string that uses the back-tick / Grave accent (`) as a 
# beginning and ending delimiter
puts `ls`

# This command output string is sent to the OS as a command to be executed.

# **This is interesting**
# You can also spawn a separate system process using the 'Kernel' class method
# 'system'
# It executs the command as a sub-process.  It returns true if the command was found
# and executed properly.  It returns false if command exited with a nonzero
# status, and 'nil' if the command failed to execute.

# Example:

system("tar xzf test.tgz") # => true

__END__

***Side Note***

Take a look at the Ruby Scripting Book.  Could be a good way to learn Ruby
to write useable, valuable system scripts for OS X.












