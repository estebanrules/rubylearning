# Numbers in Ruby

=begin
 Ruby Numbers  
 Usual operators:  
 + addition  
 - subtraction  
 * multiplication  
 / division 
=end

puts 1 + 2
puts 2 + 3

# When you do arithmetic with integers, you get integers as a 
# result (no shit)
puts 3 / 2
puts 10 - 11
puts 1.5 / 2.6

# Ruby Integers are of class Fixnum or Bignum, depending on their size.
# both class from the class Integer, and thereforce class Numeric (see chart)
# Floating-point numbers are objects of class Float.
# The classes Complex, BigDecimal and Rational are not built-in
# to Ruby but are distributed with Ruby as part of the standard library.

# Ruby's Modulus Operator
# Prints out the Remainder, unless the integers are "mixed", i.e.
# one positive, one negative in which case '1' is tacked on.
puts (5 % 3)    # prints 2
puts (-5 % 3)   # prints 1
puts (5 % -3)   # prints -1
puts (-5 % -3)  # prints -2

# Difference between || and 'or'
# The only differene is their precedence.  || has a higher precedence order  
# than 'or'.

# A common idion is to use || to assign a value to a variable only if that variable
# isn't already set.
# Example:

@variable = @variable || "default value"
# or:
@variable || ="default value"

# Why does this difference exist?  You can write a Boolean expression such as the 
# following that assigns variables to variables until it encounters the following:
if a = f(x) and b = f(y) and c = f(z) then d = g(a,b,c) end
	