# Variables and Assignment.

# To store a number in your computer's memory, you need to give it a name.

s = 'Hello World!'
x = 10

PI = 3.1416 # not the preferred way to write variables in Ruby.
puts PI

# Define local variables
my_string = 'I love my city, Pune'
puts my_string

=begin
  String Conversions in Ruby:
  .to_i => to integer
  .to_s => to string
  .to_f => to float
=end

# String Concatenation
var1 = 5;
var2 = '2'
puts var1 + var2.to_i

# << appends to a string
a = 'hello '
a<<'world.
I love this world...'
puts a

=begin
<< marks the start of the string literal
and is followed by a delimiter of your choice.
The string literal then starts from the next
new line and finishes when the delimiter
is repeated again on a line on its own.  This
is known as Here document syntax
=end

a = <<END_STR
This is the string
And a second line
END_STR

puts a
