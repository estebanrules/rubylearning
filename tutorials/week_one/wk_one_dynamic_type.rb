# Here is an example to show Ruby is dynamically typed:

x = 7          # integer
x = "house"    # string
x = 7.5        # real(float)

puts x

# In Ruby, everything you manipulate is an object

all_objects = "I love Ruby".length
puts all_objects

#-------------------------------------------------------------------
# Float is a sub class of class Numeric
# DIG is a class constant that gives precision of Float in decimal digits.
# Look at the following code:

puts Float::DIG

# output is 15

# MAX is another class constant that gives the largest Float.
# Example: 

puts Float::MAX

# output is 1.79769313486232e+308

# Look at this loop:

rice_on_square = 1

64.times do |square|
	puts "On square #{square + 1} are #{rice_on_square} grain(s)"
	rice_on_square *= 2
end

# By square 64 you are up to many trillions of grain.
# What's the point?  Ruby can deal with very large numbers.

# Don't worry about this code for now, but check it:

# puts 'I am in class = ' + self.class.to_s 
# puts 'I am an object = ' + self.to_s 
# print 'The object methods are = ' 
# puts self.private_methods.sort

__END__

***RESEARCH "self" in Ruby.***


How does it compare to Python?




