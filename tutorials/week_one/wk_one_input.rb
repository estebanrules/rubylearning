# Getting input in Ruby

# gets and chomp
# gets takes in  single line of input and \n
# chomp cuts off (chomps) the \n

puts "In which city do you live?"
STDOUT.flush 
city = gets.chomp
puts "The city is " + city

# this flushes any buffered data within 'io' to the OS.
# usage is not mandatory