# Useful Ruby methods continued:

# local_variables

# This returns the names of the current local variables in a particular context.
# *It is a reflective method and can be placed anywhere in the program, i.e.
# even on line 1.*  In the example:

a = 10
def mtd
	b = 20
end

puts local_variables

# The output is:
# a

# **Because it is reflective, it will print out ALL of the local variables in
# the program even if it is on line 1**
# Why doesn't it print out b ?  Because b is a variable inside a method (I think)

__END__

Hmm ok.  Ask Victor about printf("Number: %5.2f,\nString: %s\n", 7.18, "FORPC101")
