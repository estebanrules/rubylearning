# More Ruby methods

# A simple method
def hello
	"Hello"
end

# call the method
puts hello

#--------------------------------------------
# Method with an argument

def hello_one(name_one)
	"Hello" + name_one
end

# call the method
puts hello_one("Esteban")

#---------------------------------------------
# Another method with an argument
# As of Ruby 1.9 you do not need parentheses when declaring a method's input
def hello_two name_two
	"Hello" + name_two
end

# call the method
# As of Ruby 1.9 you do not need parentheses when calling a method.  Fuck nutty, right?
puts hello_two "PoopsMcgee"

#-------------------------------------------------------

# Ruby lets you specify default values for a method's arguments--values that will be used
# if when the method is called nothing is passed explicitly.  You can always call the 
# method later on by passing in values to the method.
def method_default(arg_one = "Dibya", arg_two = "Shashank", arg_three = "PoopsMcgee")
	"#{arg_one}, #{arg_two}, #{arg_three}"
end

# call the method without passing in any values, it will revert to the default
puts method_default
# output:  Dibya, Shashank, PoopsMcgee

# call it a second time, this time passing in one value
puts method_default("Ruby")
# output: Ruby, Shashank, PoopsMcgee
#-------------------------------------------------------------------

# Interpolation is the process of inserting the result of an expression in to a string literal.
# We do this via #{}
# Example:
puts "100 * 5 = #{100 * 5}"
# output: 100 * 5 = 100

#-----------------------------------------------------------------

# Aliasing a method
# Usage: 

#      alias new_name old_name

# This creates a new name that refers to an existing emthod.  
# When a method is aliased, the new name refers to a copy of the original
# method's body.

# BUT if the method is subsequently redefined, the alias
# name will still invoke the original implementation.

# Example:
def old_method
	"old method"
end

alias new_method old_method

def old_method
	"old improved method"
end

puts old_method
puts new_method

# The output is:

#   old improved method
#   old method
#-----------------------------------------------------

# Ruby allow us to write functions that can accept
# a variable number of parameters
def foo(*my_string)
	my_string.inspect
end

puts foo("hello", "world")
puts foo()

# The asterisk is called the "splat argument" is taking
# ALL arguments you send in to the method and assigning
# them to an array entitled "my_string".

# First Output:
# ["hello", "world"]

# Second Output:
# []

# ALSO, In Ruby 1.9 you can put the splat argument anywhere in a method's
# parameter list.
# Example:

#   def opt_args(a, *x, b)

# There is no limit to the amount of arguments you can pass in to
# a method
#----------------------------------------------------

# Another example:

def method_another(a = 99, b = a + 1)
	[a,b]
end

puts method_another

# Output: 99, 100

#-------------------------------------------------
# There are some class methods in Ruby that can be called
# with or with out the !.
# Example:

#   .downcase versus .downcase!

# In this case, the downcase method does not have a bang, 
# so it instantiates a new object with the new attributes.  
# If the variable (in this case) is referenced again, it will not retain
# that value.
def downer(string)
	string.downcase
end

a = "HELLO"

downer(a) # -> "hello"
puts a    #-> "HELLO"

# HOWEVER.....
# The bang versions of the same methods perform the action, but they do 
# so in place: INSTEAD of creating a new object, they transform the original 
# object.”

# Example:

def downer_bang(string)
	string.downcase!
end

a = "HELLO"
downer_bang(a) # -> "hello"

puts a # -> "hello"

__END__

**Go over this last bit of this chapter again.
****GO TO THE LINKS IN THIS CHAPTER!!****















































































































