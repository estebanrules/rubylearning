# encoding UTF-8
# If the first line of a file is a comment, Rubyscans it for the string "coding".
# Thus, you could use "coding" or "encoding".
# Note: Ruby writes a byte sequence \xEF\xBB\xBF at the start of a source file,
# when you use utf-8.


# encoding: utf-8
 
# λ is the Greek character Lambda here
puts "λ".length     # => 1
puts "λ".bytesize   # => 2
puts "λ".encoding   # => UTF-8