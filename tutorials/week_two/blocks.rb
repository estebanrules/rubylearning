# Blocks (read accompanying tutorial)
# Usually the code blocks passed into methods are anonymous objects, created on the spot.
# Example, the following block is associated with the call to a method great.

#    greet {puts 'Hello'}

# If the method has parameters, they appear before the block.

#    verbose_greet("PuneRuby") {puts 'Hello'}

# Associated Blocks
# A method can invoke an 'associated block' one or more time using the yield statement
# Thus, any method that wants to take a block as a parameter can use the yield 
# keyword to execute the block at any time.

=begin 
  Ruby Code blocks are chunks of code between braces or  
  between do- end that you can associate with method invocations
=end
def call_block
  puts 'Start of method'
  # you can call the block using the yield keyword
  yield
  yield
  puts 'End of Method'
end
# Code blocks may appear only in the source adjacent to a method call 
call_block {puts 'In the block'}

# You can provide parameters to the call to yield:  
# these will be passed to the block
def call_block2
  yield('hello', 99)
end
call_block2 {|str, num| puts str + '' + num.to_s}

# block_given? returns true if yield would execute a block in the current context.
# Example:
def try
  if block_given?
    yield
  else
    puts "no block"
  end
end
try # => "no block"
try {puts "hello"} # => "hello"
try do puts "hello" end # => "hello"

# Block Variables
# Let's see what happens in the following example when a variable outside a block
# is x and a block parameter is also named x
x = 10
5.times do |x|
  puts "x inside the block #{x}"
end

puts "x outside the block: #{x}"
# see accompanying tutorial for output

# You will observe that after the block has executed, x outside the block is the original x. 
# Hence the block parameter x was local to the block.

# Now see what happens to x in this example:
x = 10
5.times do |y|
  x = y
  puts "x inside the block: #{x}"
end
puts "x outside the block: #{x}"

# see accompanying tutorial for output

# Since x is not a block parameter here, the variable x is the same inside and
# outside the block.

# In Ruby 1.9 blocks introduce their own scope for the block parameters only.
# This is illustrated here:

x = 10
5.times do |y; x|
  x = y
  puts "x inside the block: #{x}"
end
puts "x outside the block: #{x}"
# in the above block, a new feature is being used: "block local variable"
# Basically, block local variables 'shield' a block from manipulating variables
# outside of its scope.

#The syntax for a block local variable is simple. Put a semicolon after the normal
# block parameter list, then list the variable you want as block local variables. 
# For example, if the block takes two variables a and b, and uses to local variables 
# x and y, the parameter list would look like this: |a,b; x,y|.

__END__

*** Go over Blocks in Beginning Ruby book.





















