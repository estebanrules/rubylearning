
# unless, as a statement or modifier is the opposite of if:
# it only runs the codd if an associated expression evaluates to false or nil.
unless  ARGV.length == 2
  puts "Usage: program.rb 23 45"
  exit
end
# Above, the code is executed unless the number of elements in the
# array is equal to 2 (means that both args were given).
# The method Kernel.exit is called, program ends.

# while loops
var = 0
while var < 10
  puts var.to_s
  var += 1
end

# Conditional ?
# ***The only ternary operator in Ruby (three operands)***

# Example:
age = 15
# We'll go in to the range class later on.
puts (13...19).include?(age) ? "teenager" : "not a teenager"

# ternary operator can also be useful for conditional assignments:
person = (13...19).include?(age) ? "teenager" : "not a teenager"
puts person # => "not a teenager"

# Statement modifiers are a shortcut.  
participants = 2501
puts "Enrollments will now STOP" if participants > 2500

# Case Expressions
# This form of conditional is fairly close to a series of if statements:
# IT LETS YOU LIST A SERIES OF CONDITIONS AND EXECUTE A STATEMENT CORRESPOMNDING
# TO THE FIRST ONE THAT'S TRUE.

# Example: Leap Years.  Leap years must be divisible by 400, or divisible by 4
# and not by 100.
year = 2000
leap = case
  when year % 400 == 0 then true
  when year % 100 == 0 then false
  else year % 4 == 0
  end
puts leap # output is: true

# nil is an Object
# Like everything in Ruby, nil is an object.  You can add methods to it, etc.

# IN RUBY, NIL AND FALSE EVALUATE TO FALSE 
# and
# EVERYTHING ELSE (INCLUDING TRUE AND 0) EVALUATE TO TRUE.

# Difference between false and nil.

# We can determine our object's class and its unique object ID
# NIL is nil
puts NIL.class # NilClass
puts nil.class # NilClass
puts nil.object_id # 4
puts NIL.object_id # 4

# FALSE is false
puts FALSE.class # FalseClass
puts false.class # FalseClass
puts false.object_id # 0
puts FALSE.object_id # 0




































