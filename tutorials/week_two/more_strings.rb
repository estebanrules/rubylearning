# Strings have many methods.
# ***There is little difference between "" and '', except that Ruby uses
# more processing power when using "".  "" strings are also used for
# string interpolation.


# Original Code
def say_goodnight(name)
  result = "Good night, #{name}"
  return result
end
puts say_goodnight('Esteban')

# What's "wrong with this code?"  There is nothing technically wrong with it,
# but in Ruby, the value returned by a method (in this case) is the value of hte last
# expression evaluated.  Guess What?  We don't need the variable result or the
# return statement at all! DRY DRY DRY DRY DRY


# Modified Code
def say_goodnight2(name)
  "Good night, #{name}"
end
puts say_goodnight2('Superman')

# Every time a string literal is used un an assignment as a parameter, a new String
# object is instantiated.

# Strings are objects of class String.  The String class has more than 75 standard
# methods.

String.methods.sort # shows a list of methods that the Class object String responds to.

String.instance_methods.sort # shows you all instance methods of String.

# Comparing strings for equality
# The class methods String.eql? and == tests for identical content
# The class method String.equal? tests whether two strings are the same object.
s1 = 'Esteban'
s2 = 'Esteban'
s3 = s1

if s1 == s2
  puts 'Both Strings have identical content.'
else
  puts 'Both Strings do not have identical content.'
end

if s1.eql?(s2)
  puts 'Both strings have identical content.'
else
  puts 'Both strings do not have identical content.'
end

if s1.equal?(s2)
  puts 'Two strings are identical objects.'
else
  puts 'Two strings are not identical objects.'
end

if s1.equal?(s3)
  puts 'Two Strings are identical objects.'
else
  puts 'Two Strings are not identical objects.'
end

# Using %w
# You can create a shortcut to make an array.
# Example:
names1 = ['ann', 'richard', 'william', 'susan', 'pat']
puts names1[0] # ann
puts names1[3] # susan
# This is the same:
names2 = %w{ann richard william susan pat}
puts names2[0] # ann
puts names2[3] # susan
