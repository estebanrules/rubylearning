# Simple constructs, if else and end.
# In Ruby, nil and false evaluate to false,
# everthing else (including true, 0) means true
# **nil is an actual object**

var = 5
if var > 4
  puts "Variable is greater than 4."
  puts "I can have multiple statements here."
  if var == 5
    puts "Nested if else statements are possible."
  else
    puts "Too cool."
  end
else 
  puts "Variable is not greater than 5."
  puts "I can have multiple statements here."
end

# Example of else/if vs. elsif 
puts "Hello, what's your name?"
STDOUT.flush
name = gets.chomp
puts 'Hello, ' + name + '.'

if name == 'Esteban'
  puts 'Esteban, you are amazing.'
else
  if name == 'Superman'
    puts 'Superman, you are cool too.'
  end
end

# Modified version with elsif 
puts "Hello, what's your name?"
STDOUT.flush
name = gets.chomp
puts 'Hello, ' + name + '.'

if name == 'Esteban'
  puts 'Esteban, you are so amazing.'
elseif name == 'Superman'
  puts 'Superman, you are cool too.'
end

# Further modified with || - || is the logical or operator.
puts "Hello, what's your name?"  
STDOUT.flush  
name = gets.chomp  
puts 'Hello, ' + name + '.' 

if name == 'Esteban' || name == 'Superman'
  puts 'Both of you guys are great.'
end

# Other common conditional operators are: ==. !=, >=, <=, >, <





































  